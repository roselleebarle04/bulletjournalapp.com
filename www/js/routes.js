app.config(function ($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // Accounts
    ////////////////////

    .state('welcome', {
      url: '/welcome',
      controller: 'main',
      templateUrl: 'templates/welcome.html'
    })

    .state('login', {
      url: '/login',
      controller: 'main',
      templateUrl: 'templates/login.html'
    })

    .state('signup', {
      url: '/signup',
      controller: 'main',
      templateUrl: 'templates/signup.html'
    })

    .state('profile', {
      url: '/profile',
      controller: 'main',
      templateUrl: 'templates/profile.html'
    })

    .state('index', {
      url: '/',
      controller: 'main',
      templateUrl: 'templates/home.html'
    })

    // Modules
    ////////////////////

    .state('module-index', {
      url: '/index',
      controller: 'main',
      templateUrl: 'templates/modules/index.html'
    })

    .state('module-future', {
      url: '/future',
      controller: 'main',
      templateUrl: 'templates/modules/future.html'
    })

    .state('module-monthly', {
      url: '/monthly',
      controller: 'main',
      templateUrl: 'templates/modules/monthly.html'
    })

    .state('module-daily', {
      url: '/daily',
      controller: 'main',
      templateUrl: 'templates/modules/daily.html'
    });

  // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/');


});
