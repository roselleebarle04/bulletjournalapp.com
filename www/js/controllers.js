app.controller('main', function ($scope, $location, $ionicSideMenuDelegate, Bullet, Topic) { //store the entities name in a variable var
  $scope.topics = [];
  $scope.topic = {};

  $scope.bullets = [];

  $scope.initialize = function () {
    Topic.get(function (data) {
      $scope.topics = data.topics;
    });

    Topic.get({topicid: 1}, function (data) {
      $scope.topic = data.topics[0];
      console.log($scope.topic);
    });

    Bullet.get({topicid: 1}, function (data) {
      $scope.bullets = data.bullets;
      console.log($scope.bullets);
    });
  };

  $scope.toggleLeft = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.initialize();
});
