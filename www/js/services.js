app.factory("Topic", function ($resource) {
  return $resource("http://localhost:5000/topics/:topicid/", {topicid: '@topicid'}, {
    'update': {method: 'PUT'}
  });
});

app.factory("Bullet", function ($resource) {
  return $resource("http://localhost:5000/topics/:topicid/bullets/:bulletid", {
    topicid: '@topicid',
    bulletid: '@bulletid'
  }, {
    'update': {method: 'PUT'}
  });
});
